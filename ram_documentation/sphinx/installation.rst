============
Installation
============

Dépendances
===========
Ce paquet a été testé avec Ubuntu 16.04 et `ROS Kinetic <http://wiki.ros.org/kinetic>`_, Ubuntu 18.04 et `ROS Melodic <http://wiki.ros.org/melodic>`_.

VTK 7.1
-------
Il faut VTK 7.1 ou plus pour compiler le logiciel.

Voici quelques étapes rapides pour télécharger, compiler la dernière version de VTK:

.. code-block:: bash

  mkdir –p $HOME/libraries/VTK-8.1/build_release
  cd $HOME/libraries/VTK-8.1/
  wget https://www.vtk.org/files/release/8.1/VTK-8.1.1.zip
  unzip VTK-8.1.1.zip
  mv VTK-8.1.1 src
  cd build_release
  cmake ../src – DCMAKE_BUILD_TYPE=Release
  make – j4

Installer avec:

.. code-block:: bash

  sudo make –j4 install
  sudo ldconfig

wstool
------
Installer `wstool <http://wiki.ros.org/wstool>`_

rosdep
------
Installer, initialiser et mettre à jour `rosdep <http://wiki.ros.org/rosdep>`_

Compiler
========
Créer un espace de travail catkin et cloner le projet:

.. code-block:: bash

  mkdir –p catkin_workspace
  cd catkin_workspace
  git clone https://gitlab.com/InstitutMaupertuis/ros_additive_manufacturing.git
  wstool init src ros_additive_manufacturing/ros_additive_manufacturing.rosinstall
  mv ros_additive_manufacturing src

Résoudre les dépendances ROS
----------------------------
.. code-block:: bash

  rosdep install --from-paths src --ignore-src --rosdistro kinetic –y

Lancer la compilation
---------------------
Utiliser ``catkin`` pour compiler:

.. code-block:: bash

  catkin_make

Lancer l'application
====================
`Sourcer l’espace de travail catkin <http://wiki.ros.org/ROS/Tutorials/InstallingandConfiguringROSEnvironment#Create_a_ROS_Workspace>`_ dans lequel vous avez compilé les paquets et lancer:

.. code-block:: bash

  roslaunch ram_qt_guis gui.launch

Si l’application fonctionne correctement, passer à l’étape d’installation, autrement, contacter le support, :ref:`Contact`.

Installer
=========
.. code-block:: bash

  catkin_make install

Lancer le logiciel une première fois avec:

.. code-block:: bash

  gtk-launch ros_additive_manufacturing

Il est désormais possible d’utiliser le lanceur d’applications pour lancer l’application sans ligne de commande.
