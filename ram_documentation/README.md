# Generating the documentation

## Dependencies
```bash
sudo apt update
sudo apt install -y python-pip latexmk texlive-base texlive-latex-recommended texlive-fonts-recommended texlive-latex-extra
sudo pip install --no-cache-dir sphinx==1.7.4 sphinx_rtd_theme
```

## Generating
```bash
catkin_make -Dram_documentation_GENERATE=ON
```

To see the generated documentation, go in the `build/ros_additive_manufacturing/ram_documentation` directory and in the `latex` or `html` directories.
- Open the `ROSAdditiveManufacturing.pdf` file.
- Open the `index.html` file with a web browser.

The HTML documentation is installed when using `catkin_make install` (only if the documentation is generated).
